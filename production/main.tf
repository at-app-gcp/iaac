terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {
  version = "3.5.0"
  credentials = file(var.credentials)
  project = "${var.project}"
  region  = "${var.region}"
  zone    = "${var.zone}"
}

module "cloud_run" {
  source = "../modules/cloud_run"
  cloud_run_name = "client" 
  location = "${var.region}"
  cloud_run_image = "gcr.io/${var.project}/client"
}

# module "compute" {
#   source = "../modules/compute"
#   compute_name = "server"
#   zone = "${var.zone}"
#   compute_image = "gcr.io/${var.project}/server"
#   compute_machine_type = "standart"
#   zone = "${var.zone}"
# }

# module "cloud_function" {
#   source = "../modules/cloud_function"
#   function_name = "oanda_update_candles"
#   function_description = "updating candles"
#   function_runtime = "node10js"
#   function_memory = 128
#   function_entry_point = "run"
#   local_path = "../../functions/oanda_update_candles/index.zip"
#   bucket_archive_name = "oanda_update_candles_archive"
#   bucket_name = "oanda_update_candles_bucket"
# }


# ********************************************
# oanda_update_candles
# ********************************************


resource "google_storage_bucket" "bucket20" {
  name = "oanda_update_candles-bucket20"
}

resource "google_storage_bucket_object" "archive20" {
  name   = "oanda_update_candles.zip"
  bucket = google_storage_bucket.bucket20.name
  source = "../functions/oanda_update_candles.zip"
}

resource "google_cloudfunctions_function" "function20" {
  name        = "oanda_update_candles"
  description = "oanda_update_candles cloud function"
  runtime     = "nodejs10"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.bucket20.name
  source_archive_object = google_storage_bucket_object.archive20.name
  trigger_http          = true
  entry_point           = "run"
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker2" {
  project        = google_cloudfunctions_function.function20.project
  region         = google_cloudfunctions_function.function20.region
  cloud_function = google_cloudfunctions_function.function20.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}

# ********************************************
# oanda_update_account
# ********************************************


resource "google_storage_bucket" "bucket21" {
  name = "oanda_update_account-bucket21"
}

resource "google_storage_bucket_object" "archive21" {
  name   = "oanda_update_account.zip"
  bucket = google_storage_bucket.bucket21.name
  source = "../functions/oanda_update_account.zip"
}

resource "google_cloudfunctions_function" "function21" {
  name        = "oanda_update_account"
  description = "oanda_update_account cloud function"
  runtime     = "nodejs10"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.bucket21.name
  source_archive_object = google_storage_bucket_object.archive21.name
  trigger_http          = true
  entry_point           = "run"
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.function21.project
  region         = google_cloudfunctions_function.function21.region
  cloud_function = google_cloudfunctions_function.function21.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}






# module "cloud_function" {
#   source = "../modules/cloud_function"
#   function_name = "oanda_create_order"
#   function_description = "creating orders"
#   function_runtime = "node10js"
#   function_memory = 128
#   function_entry_point = "run"
#   function_source = "../../functions/oanda_create_order/index.zip"
#   bucket_archive_name = "oanda_create_order_archive"
#   bucket_name = "oanda_create_order_bucket"
# }

# module "cloud_function" {
#   source = "../modules/cloud_function"
#   function_name = "oanda_get_account"
#   function_description = "getting account details"
#   function_runtime = "node10js"
#   function_memory = 128
#   function_entry_point = "run"
#   function_source = "../../functions/oanda_get_account/index.zip"
#   bucket_archive_name = "oanda_get_account_archive"
#   bucket_name = "oanda_get_account_bucket"
# }

# module "cloud_function" {
#   source = "../modules/cloud_function"
#   function_name = "oanda_get_trade"
#   function_description = "getting trade"
#   function_runtime = "node10js"
#   function_memory = 128
#   function_entry_point = "run"
#   function_source = "../../functions/oanda_get_trade/index.zip"
#   bucket_archive_name = "oanda_get_trade_archive"
#   bucket_name = "oanda_get_trade_bucket"
# }


