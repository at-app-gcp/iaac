```

at-app-gcp/
   client/
   |   .dockerignore
   |   .env.local
   |   .env.production
   |   .gitignore
   |   .gitlab-ci.yml
   |   debug.log
   |   Dockerfile
   |   git_setup.sh
   |   git_upload.sh
   |   nginx.conf
   |   package-lock.json
   |   package.json
   |   README.md
   |   tree.doc
   |   tsconfig.json
   |   yarn.lock
   |   
   +---public
   |       favicon.ico
   |       index.html
   |       logo192.png
   |       logo512.png
   |       manifest.json
   |       robots.txt
   |       
   \---src
       |   index.css
       |   index.tsx
       |   react-app-env.d.ts
       |   
       +---contexts
       |   \---AuthContext
       |           index.tsx
       |           
       +---fonts
       |       asd.sd
       |       ProductSans-Black.ttf
       |       ProductSans-BlackItalic.ttf
       |       ProductSans-Bold.ttf
       |       ProductSans-BoldItalic.ttf
       |       ProductSans-Italic.ttf
       |       ProductSans-Light.ttf
       |       ProductSans-LightItalic.ttf
       |       ProductSans-Medium.ttf
       |       ProductSans-MediumItalic.ttf
       |       ProductSans-Regular.ttf
       |       ProductSans-Thin.ttf
       |       ProductSans-ThinItalic.ttf
       |       Roboto-Black.ttf
       |       Roboto-BlackItalic.ttf
       |       Roboto-Bold.ttf
       |       Roboto-BoldItalic.ttf
       |       Roboto-Italic.ttf
       |       Roboto-Light.ttf
       |       Roboto-LightItalic.ttf
       |       Roboto-Medium.ttf
       |       Roboto-MediumItalic.ttf
       |       Roboto-Regular.ttf
       |       Roboto-Thin.ttf
       |       Roboto-ThinItalic.ttf
       |       
       +---routes
       |   +---Firebase
       |   |       index.tsx
       |   |       
       |   +---Login
       |   |       index.tsx
       |   |       
       |   +---PrivateRoute
       |   |       index.tsx
       |   |       
       |   \---Router
       |           CustomDiv.tsx
       |           index.tsx
       |           routes.tsx
       |           
       \---views
           +---Home
           |   |   index.tsx
           |   |   
           |   \---components
           |       +---AccountCard
           |       |       index.tsx
           |       |       
           |       +---HomeCard
           |       |       index.tsx
           |       |       
           |       \---SystemCard
           |               index.tsx
           |    
           +---Home
           |   |   index.tsx
           |
           +---Errors
           |   |   index.tsx       
           |                          
           +---Profile
           |   |   index.tsx
           |   |   
           |   \---components
           |       \---User
           |               index.tsx
           |               
           +---Trades
           |   |   index.tsx
           |   |   
           |   \---components
           |       \---Trade
           |               index.tsx
           |               
           \---Workers
               |   index.tsx
               |   
               \---components
                   \---Worker
                           index.tsx
   
   database/
   |   .gitignore
   |   .gitlab-ci.yml
   |   Dockerfile
   |   git_setup.sh
   |   git_upload.sh
   |   main.py
   |   README.md
   |   requirements.txt
   |   tree.doc
   |   __init__.py
   |   
   +---api
   |   |   at202xcloud-0d0f6e4aace8.json
   |   |   __init__.py
   |   |   
   |   \---__pycache__
   |           __init__.cpython-38.pyc
   |           
   \---state
       |   __init__.py
       |   
       \---__pycache__
               __init__.cpython-38.pyc
 
   functions/
   |  oanda_create_order/
   |  oanda_get_trade/
   |  oanda_update_account/
   |  oanda_update_candles/
 
   iaac/
   |   README.md
   |   tree.doc
   |   
   +---development
   |   |   main.tf
   |   |   outputs.tf
   |   |   terraform.exe
   |   |   terraform.tfstate
   |   |   terraform.tfstate.backup
   |   |   variables.tf
   |                                   
   +---functions
   |       oanda_create_order.zip
   |       oanda_get_trade.zip
   |       oanda_update_account.zip
   |       oanda_update_candles.zip
   |       
   +---modules
   |   +---cloud_function
   |   |       main.tf
   |   |       outputs.tf
   |   |       variables.tf
   |   |       
   |   +---cloud_run
   |   |       main.tf
   |   |       variables.tf
   |   |       
   |   +---compute
   |   |       main.tf
   |   |       startup_script.sh
   |   |       variables.tf
   |   |       
   |   +---terraform-google-cloud-function
   |   |   |   .dockerignore
   |   |   |   .gitignore
   |   |   |   .ruby-version
   |   |   |   CHANGELOG.md
   |   |   |   CONTRIBUTING.md
   |   |   |   Gemfile
   |   |   |   kitchen.yml
   |   |   |   LICENSE
   |   |   |   main.tf
   |   |   |   Makefile
   |   |   |   outputs.tf
   |   |   |   README.md
   |   |   |   variables.tf
   |   |   |   versions.tf
   |   |   |   
   |   |                       
   |   \---terraform-google-container-vm
   |       |   .dockerignore
   |       |   .gitignore
   |       |   .kitchen.yml
   |       |   CHANGELOG.md
   |       |   CONTRIBUTING.md
   |       |   LICENSE
   |       |   main.tf
   |       |   Makefile
   |       |   output.tf
   |       |   README.md
   |       |   requirements.txt
   |       |   variables.tf
   |       |   versions.tf
   |          
   \---production
           .gitignore
           at202xcloud-e8a6266f9a57.json
           git_setup.sh
           git_upload.sh
           main.tf
               
       
   server/
   |   .gitignore
   |   .gitlab-ci.yml
   |   Dockerfile
   |   git_setup.sh
   |   git_upload.sh
   |   main.py
   |   README.md
   |   requirements.txt
   |   tree.doc
   |   __init__.py
   |   
   +---api
   |   |   1.py
   |   |   2.py
   |   |   at202xcloud-0d0f6e4aace8.json
   |   |   __init__.py
   |   |   
   |   \---__pycache__
   |           __init__.cpython-38.pyc
   |           
   +---helpers
   |   |   __init__.py
   |   |   
   |   \---__pycache__
   |           __init__.cpython-38.pyc
   |           
   \---worker
       |   __init__.py
       |   
       \---__pycache__
               __init__.cpython-38.pyc
``` 



<!-- OLD -->
writes : 7,152 20:50
8,232 21:00
9,411 21:11
10,382 21:20
12,530 21:40
13,595 21:50
100 in 1 min
1080 in 10 mins 
3 230 in 30 mins
6 443 - 6 460 in 60 mins 

24 workers n document fields writes every 10 seconds in 120h =  775 200 - 777 600 writes
 777 600 - 100 000 (5 days free quota 20k)
$0.117 per 100,000 documents
0,909792 $ for 777 600 writes
0,792792 $ for 677 600 writes = 120h x 10secs/loop - 5 days free quota 20k (-100k)

max loop iters for 120 h @ 10 secs/loop = 43 200

10 secs/loop
4,4 times loop execution per minute ; max:6  
268,45 times loop execution per hour ; max:360
6 442 times loop execution per day : max 8640 
32215    times loop execution per 120 hours; max:43 200
128860 times loop execution per 120 hours x 4; max:172 800

Net price for 24 workers 10 sec/loop
0,792792 $ @ one week (120 h) @ 32,215 exections
3,171168 $ @ one month (120h * 4) @ 128,860 executions

Net price for 240 workers 10 sec/loop   
7,92792 $ @ one week (120 h); 322,150
31.71168 $ @ one month (120h * 4)  1,288,600


#  For writes, each set or update operation counts as a single write.
<!-- OLD -->



                
const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.firestore();

For Windows User:
Follow Instructions:
Control Panel >> User Account >> Credential Manager >> Windows Credential >> Generic Credential

Deployemnt of Cloud run :
https://www.sethvargo.com/configuring-cloud-run-with-terraform/
1. npm run build
2. Gitlab repo
3. IAM service account for CI/CD Gitlab deployment

sada

export PROJECT=ProjectName
export TF_VAR_billing_account=xxxxxx-xxxxxx-xxxxxx
export TF_VAR_project=${PROJECT}-terraform
export TF_CREDS=./${PROJECT}-terraform.json

gcloud projects create ${TF_VAR_project} \
  --set-as-default

gcloud beta billing projects link ${TF_VAR_project} \
  --billing-account ${TF_VAR_billing_account}

######################################################################################
##### Create the Terraform service account
######################################################################################

gcloud iam service-accounts create terraform \
  --display-name "Terraform admin account"

gcloud iam service-accounts keys create ${TF_CREDS} \
  --iam-account terraform@${TF_VAR_project}.iam.gserviceaccount.com

gcloud projects add-iam-policy-binding ${TF_VAR_project} \
  --member serviceAccount:terraform@${TF_VAR_project}.iam.gserviceaccount.com \
  --role roles/editor

gcloud projects add-iam-policy-binding ${TF_VAR_project} \
  --member serviceAccount:terraform@${TF_VAR_project}.iam.gserviceaccount.com \
  --role roles/storage.admin

gcloud projects add-iam-policy-binding ${TF_VAR_project} \
  --member serviceAccount:terraform@${TF_VAR_project}.iam.gserviceaccount.com \
  --role roles/appengine.appAdmin


gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable serviceusage.googleapis.com

######################################################################################
##### Set up remote state in Cloud Storage
######################################################################################

gsutil mb -p ${TF_VAR_project} gs://${TF_VAR_project}

cat > backend.tf << EOF
terraform {
 backend "gcs" {
   bucket  = "${TF_VAR_project}"
   prefix  = "terraform/state"
 }
}
EOF

gsutil versioning set on gs://${TF_VAR_project}

export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_VAR_project}


variable "project" {}
variable "region" {}

provider "google" {
    project = var.project
    region = var.region
}

data "google_project" "project" {}

resource "google_project_service" "service" {
  for_each = toset([
    "appengine.googleapis.com",
    "appengineflex.googleapis.com",
    "firestore.googleapis.com"
  ])

  service = each.key

  disable_on_destroy = false
}

resource "google_app_engine_application" "app" {
    project     = data.google_project.project.project_id
    location_id = var.region
}
When I run the terraform plan and apply the resources 'service' are created correcly but the app engine application cannot be created and I get the follwoing error:

google_app_engine_application.app: Creating...

Error: Error creating App Engine application: googleapi: Error 403: The caller does not have permission, forbidden

  on project.tf line 24, in resource "google_app_engine_application" "app":
  24: resource "google_app_engine_application" "app" {




https://stackoverflow.com/questions/43324649/react-router-not-working-after-building-the-app

https://stackoverflow.com/questions/27928372/react-router-urls-dont-work-when-refreshing-or-writing-manually

https://stackoverflow.com/questions/57572259/react-router-with-relative-path-deployment

https://dev.to/nburgess/creating-a-react-app-with-react-router-and-an-express-backend-33l3

https://medium.com/@bennirus/deploying-a-create-react-app-with-routing-to-github-pages-f386b6ce84c2

 tree my-company-functions
├── document-metadata
│   └── main.tf
├── document-translate
│   └── main.tf
└── modules
    ├── function
    │   ├── main.tf      // contains aws_iam_role, aws_lambda_function
    │   ├── outputs.tf
    │   └── variables.tf
    ├── queue
    │   ├── main.tf      // contains aws_sqs_queue
    │   ├── outputs.tf
    │   └── variables.tf
    └── vnet
        ├── main.tf      // contains aws_vpc, aws_subnet
        ├── outputs.tf
        └── variables.tf

        https://www.hashicorp.com/blog/structuring-hashicorp-terraform-configuration-for-production


module "vnet" {
 source     = "../modules/vnet"
 cidr_block = "10.0.0.0/16"
}

module "queue" {
 source                    = "../modules/queue"
 name                      = "terraform-example-queue"
 delay_seconds             = 90
 max_message_size          = 2048
 message_retention_seconds = 86400
 receive_wait_time_seconds = 10
}

module "function" {
 source           = "../modules/function"
 filename         = "lambda_function_payload.zip"
 function_name    = "lambda_function_name"
 role             = aws_iam_role.iam_for_lambda.arn
 handler          = "exports.test"
 source_code_hash = filebase64sha256("lambda_function_payload.zip")
 runtime          = "nodejs8.10"
}










https://registry.terraform.io/modules/terraform-google-modules/project-factory/google/latest


CREATE FUNCTION
Function name : <BROKER>_<method>_<data>
Region : europe-west-3
Trigger type : HTTP
Trigger URL : https://<region>-at-project-<date>.cloudfunctions.net/<function_name>
Require authentication : True
Service account : App Engine default service account 

PERMISSIONS 
New members : allUsers
Select a role : Cloud Functions > Cloud Functions Invoker

CREATE NEW PROJECT
Project name : at-project-<date>
Pin : 
    Cloud functions
    App Engine
    Compute Engine
    Firestore
Create service account and generate serviceAccount JSON

**************************************************************

Inception:
git clone https://github.com/fvrlak/at-client-template 

Engine:
App Engine standard environment instances
Iowa
Instance Type: F1
Instance Hours: 730 per month
USD 0.00

Stack:
React 16.14.0
React Router DOM 5.2.0 
JavaScript

*******************************************

INCEPTION:
git clone https://github.com/fvrlak/at-server-template 

ENGINE:

f1-micro (1 vCPU, 614 MB memory)
+ 1GB SWAP file 
us-central1-a(Iowa)
Item	Estimated costs:
$0.006 in 1 hour
$0.14 in 1 day
$0.072 in 5 days
$2.88 in 20 days
1 shared vCPU + 0.6 GB memory	$5.55/month
10 GB standard persistent disk	$0.40/month
Sustained use discount - $1.66/month
Total	$4.28/month ~ 730 hours FREE

g1-small (1 vCPU. 1.7 GB memory)
+ 1GB SWAP file
us-central1-a(Iowa)
Item	Estimated costs:
$0.019 in 1 hour
$0.456 in 1 day
$2.28 in 5 days
$9.12 in 20 days
1 shared vCPU + 1.7 GB memory	$19.71/month
10 GB standard persistent disk	$0.40/month
Sustained use discount - $5.91/month
Total	$14.20/month

STACK:
Python 3.8
Docker 19.03.09


**********
Build docker image for at-server-<date> : 
docker build -t at-server-<date> .
docker tag <image_id> fvrlak/at-server-<date>:latest
docker push fvrlak/at-server-<date>

Create Compute Engine:

Name = compute-engine-server-<date>
Region = us-central1(Iowa)
Zone = us-central1-a
Series = N1
Machine type = f1-micro (1 vCPU, 614 MB memory) || g1-small (1vCPU, 1.7 GB memory) 
Deploy a container image to this VM instance : True 
Run as privileged : True
Allocate a pseudo-TTY : True
Boot disk Container-Optimized OS 85-13310.1041.24 stable 10GB - Standard
Allow HTTP traffic : True
Allow HTTPS traffic : True

Container image = gcr.io/<project_id>/at-server-<date>:latest

gcloud compute scp --project <project_id> --zone <zone> --recurse <instance_name>:/path/to/logfile ./
sudo docker cp <container_id>:path/to/logfile
export log.txt file in Cloud Shell UI

Deploy docker image to gcr:

gcloud auth configure-docker
docker tag <image_id> gcr.io/<project_id>/<image>
docker push gcr.io/<project_id>/<image>
*************


















asd
SSH into compute instance

Download logfile from Cloud shell 
  
 


 ********************************************
 oanda_update_candles
 ********************************************


 resource "google_storage_bucket" "bucket20" {
   name = "oanda_update_candles-bucket20"
 }

 resource "google_storage_bucket_object" "archive20" {
   name   = "oanda_update_candles.zip"
   bucket = google_storage_bucket.bucket20.name
   source = "../functions/oanda_update_candles.zip"
 }

 resource "google_cloudfunctions_function" "function20" {
   name        = "oanda_update_candles"
   description = "oanda_update_candles cloud function"
   runtime     = "nodejs10"

   available_memory_mb   = 128
   source_archive_bucket = google_storage_bucket.bucket20.name
   source_archive_object = google_storage_bucket_object.archive20.name
   trigger_http          = true
   entry_point           = "run"
 }

 # IAM entry for all users to invoke the function
 resource "google_cloudfunctions_function_iam_member" "invoker20" {
   project        = google_cloudfunctions_function.function20.project
   region         = google_cloudfunctions_function.function20.region
   cloud_function = google_cloudfunctions_function.function20.name

   role   = "roles/cloudfunctions.invoker"
   member = "allUsers"
 }

 ********************************************
 oanda_update_account
 ********************************************


 resource "google_storage_bucket" "bucket21" {
   name = "oanda_update_account-bucket21"
 }

 resource "google_storage_bucket_object" "archive21" {
   name   = "oanda_update_account.zip"
   bucket = google_storage_bucket.bucket21.name
   source = "../functions/oanda_update_account.zip"
 }

 resource "google_cloudfunctions_function" "function21" {
   name        = "oanda_update_account"
   description = "oanda_update_account cloud function"
   runtime     = "nodejs10"

   available_memory_mb   = 128
   source_archive_bucket = google_storage_bucket.bucket21.name
   source_archive_object = google_storage_bucket_object.archive21.name
   trigger_http          = true
   entry_point           = "run"
 }

 # IAM entry for all users to invoke the function
 resource "google_cloudfunctions_function_iam_member" "invoker21" {
   project        = google_cloudfunctions_function.function21.project
   region         = google_cloudfunctions_function.function21.region
   cloud_function = google_cloudfunctions_function.function21.name

   role   = "roles/cloudfunctions.invoker"
   member = "allUsers"
 }


 ********************************************
 oanda_create_order2
 ********************************************


 resource "google_storage_bucket" "bucket23" {
   name = "oanda_create_order-bucket23"
 }

 resource "google_storage_bucket_object" "archive23" {
   name   = "oanda_create_order.zip"
   bucket = google_storage_bucket.bucket23.name
   source = "../functions/oanda_create_order.zip"
 }

 resource "google_cloudfunctions_function" "function23" {
   name        = "oanda_create_order2"
   description = "oanda_create_order2 cloud function"
   runtime     = "nodejs10"

   available_memory_mb   = 128
   source_archive_bucket = google_storage_bucket.bucket23.name
   source_archive_object = google_storage_bucket_object.archive23.name
   trigger_http          = true
   entry_point           = "run"
 }

 # IAM entry for all users to invoke the function
 resource "google_cloudfunctions_function_iam_member" "invoker" {
   project        = google_cloudfunctions_function.function23.project
   region         = google_cloudfunctions_function.function23.region
   cloud_function = google_cloudfunctions_function.function23.name

   role   = "roles/cloudfunctions.invoker"
   member = "allUsers"
 }

 ********************************************
 oanda_get_trade
 ********************************************


 resource "google_storage_bucket" "bucket24" {
   name = "oanda_get_trade-bucket24"
 }

 resource "google_storage_bucket_object" "archive24" {
   name   = "oanda_get_trade.zip"
   bucket = google_storage_bucket.bucket24.name
   source = "../functions/oanda_get_trade.zip"
 }
 
 resource "google_cloudfunctions_function" "function24" {
   name        = "oanda_get_trade"
   description = "oanda_get_trade cloud function"
   runtime     = "nodejs10"
   available_memory_mb   = 128
   source_archive_bucket = google_storage_bucket.bucket24.name
   source_archive_object = google_storage_bucket_object.archive24.name
   trigger_http          = true
   entry_point           = "run"
 }

 # IAM entry for all users to invoke the function
 resource "google_cloudfunctions_function_iam_member" "invoker" {
   project        = google_cloudfunctions_function.function24.project
   region         = google_cloudfunctions_function.function24.region
   cloud_function = google_cloudfunctions_function.function24.name
   role   = "roles/cloudfunctions.invoker"
   member = "allUsers"
 }

 module "cloud_function" {
   source = "../modules/cloud_function"
   function_name = "oanda_create_order"
   function_description = "creating orders"
   function_runtime = "node10js"
   function_memory = 128
   function_entry_point = "run"
   function_source = "../../functions/oanda_create_order/index.zip"
   bucket_archive_name = "oanda_create_order_archive"
   bucket_name = "oanda_create_order_bucket"
 }

 module "cloud_function" {
   source = "../modules/cloud_function"
   function_name = "oanda_get_account"
   function_description = "getting account details"
   function_runtime = "node10js"
   function_memory = 128
   function_entry_point = "run"
   function_source = "../../functions/oanda_get_account/index.zip"
   bucket_archive_name = "oanda_get_account_archive"
   bucket_name = "oanda_get_account_bucket"
 }

 module "cloud_function" {
   source = "../modules/cloud_function"
   function_name = "oanda_get_trade"
   function_description = "getting trade"
   function_runtime = "node10js"
   function_memory = 128
   function_entry_point = "run"
   function_source = "../../functions/oanda_get_trade/index.zip"
   bucket_archive_name = "oanda_get_trade_archive"
   bucket_name = "oanda_get_trade_bucket"
 }


ERRROR:
<!-- SOLUTION : destroy and apply again -->
Error: Error waiting for Creating CloudFunctions Function: Error code 13, message: Function deployment failed due to a health check failure. This 
usually indicates that your code was built successfully but failed during a test execution. Examine the logs to determine the cause. Try deploying again in a few minutes if it appears to be transient.

  on ..\modules\cloud_function\main.tf line 53, in resource "google_cloudfunctions_function" "main":
  53: resource "google_cloudfunctions_function" "main" {


    Computer Information:
    Manufacturer:  Dell Inc.
    Model:  Latitude 7490
    Form Factor: Laptop
    Touch Input Detected

Processor Information:
    CPU Vendor:  GenuineIntel
    CPU Brand:  Intel(R) Core(TM) i5-8350U CPU @ 1.70GHz
    CPU Family:  0x6
    CPU Model:  0x8e
    CPU Stepping:  0xa
    CPU Type:  0x0
    Speed:  1896 Mhz
    8 logical processors
    4 physical processors
    HyperThreading:  Supported
    FCMOV:  Supported
    SSE2:  Supported
    SSE3:  Supported
    SSSE3:  Supported
    SSE4a:  Unsupported
    SSE41:  Supported
    SSE42:  Supported
    AES:  Supported
    AVX:  Supported
    AVX2:  Supported
    AVX512F:  Unsupported
    AVX512PF:  Unsupported
    AVX512ER:  Unsupported
    AVX512CD:  Unsupported
    AVX512VNNI:  Unsupported
    SHA:  Unsupported
    CMPXCHG16B:  Supported
    LAHF/SAHF:  Supported
    PrefetchW:  Unsupported

Operating System Version:
    Windows 10 (64 bit)
    NTFS:  Supported
    Crypto Provider Codes:  Supported 311 0x0 0x0 0x0

Video Card:
    Driver:  Intel(R) UHD Graphics 620
    DirectX Driver Name:  igdumdim32.dll
    Driver Version:  26.20.100.6888
    DirectX Driver Version:  26.20.100.6888
    Driver Date: 5 16 2019
    OpenGL Version: 4.6
    Desktop Color Depth: 32 bits per pixel
    Monitor Refresh Rate: 59 Hz
    DirectX Card: Intel(R) UHD Graphics 620
    VendorID:  0x8086
    DeviceID:  0x5917
    Revision:  0x7
    Number of Monitors:  1
    Number of Logical Video Cards:  1
    No SLI or Crossfire Detected
    Primary Display Resolution:  1920 x 1080
    Desktop Resolution: 1920 x 1080
    Primary Display Size: 20.75" x 11.65" (23.78" diag)
                                            52.7cm x 29.6cm (60.4cm diag)
    Primary Bus Type Not Detected
    Primary VRAM: 1024 MB
    Supported MSAA Modes:  2x 4x 8x 16x 

Sound card:
    Audio device: Headphones (MDR-ZX330BT Stereo)

Memory:
    RAM:  16259 Mb

VR Hardware:
    VR Headset: None detected

Miscellaneous:
    UI Language:  English
    Media Type:  Undetermined
    Total Hard Disk Space Available:  238589 Mb
    Largest Free Hard Disk Block:  60810 Mb
    OS Install Date: led 01 1970
    Game Controller: None detected
    MAC Address hash: 36d12e97f1fcfa4bf66ec66e8ad281346516518e
    Disk serial number hash: 2e2906ee


 -->
