variable "region" {
  default = "europe-west3"
}

variable "project" {
  default = "at202xcloud"
}

variable "zone" {
  default = "europe-west3-c"
}

variable "cr_name" {
  default = "gcrun15787875asdsadasdasdw"
}

variable "cr_image" {
  default = "gcr.io/at202xcloud/at-iaac-client"
}

variable "credentials" {
  default = "../production/at202xcloud-e8a6266f9a57.json"
}