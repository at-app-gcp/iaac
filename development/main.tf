terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {
  version     = "3.5.0"
  credentials = file(var.credentials)
  project     = "${var.project}"
  region      = "${var.region}"
  zone        = "${var.zone}"
}

module "cloud_run1" {
   source          = "../modules/cloud_run"
   cloud_run_name  = "client"
   location        = "${var.region}"
   cloud_run_image = "gcr.io/${var.project}/client:latest"
 }

# module "cloud_run2" {
#    source          = "../modules/cloud_run"
#    cloud_run_name  = "server"
#    location        = "${var.region}"
#    cloud_run_image = "gcr.io/${var.project}/server:latest"
#  }

# module "cloud_run3" {
#    source          = "../modules/cloud_run"
#    cloud_run_name  = "database"
#    location        = "${var.region}"
#    cloud_run_image = "gcr.io/${var.project}/database:latest"
#  } 

# module "compute_1" {
#   source          = "../modules/terraform-google-container-vm/examples/simple_instance"
#   instance_name   = "server"
#   container_image = "gcr.io/at202xcloud/server:latest"
#   machine_type    = "e2-small"

# }

module "compute_2" {
  source          = "../modules/terraform-google-container-vm/examples/simple_instance"
  instance_name   = "database"
  container_image = "gcr.io/at202xcloud/database:latest"
  machine_type    = "f1-micro"
  zone = "europe-west3-a"
  project_id = var.project
  subnetwork_project = "at202xcloud"
}

# module "cloud_function_1" {
#   source               = "../modules/cloud_function"
#   bucket_name          = "oanda_create_order_bucket"
#   bucket_archive_name  = "oanda_create_order_archive"
#   local_path           = "../functions/oanda_create_order.zip"
#   function_name        = "oanda_create_order"
#   function_runtime     = "nodejs10"
#   function_memory      = 128
#   function_entry_point = "run"
#   source_dir = "${cloud_function_1.function_name}"
# }

# module "cloud_function_2" {
#   source               = "../modules/cloud_function"
#   bucket_name          = "oanda_get_trade_bucket"
#   bucket_archive_name  = "oanda_get_trade_archive"
#   local_path           = "../functions/oanda_get_trade.zip"
#   function_name        = "oanda_get_trade"
#   function_runtime     = "nodejs10"
#   function_memory      = 128
#   function_entry_point = "run"
#   source_dir = "${cloud_function_2.function_name}"

# }

module "cloud_function_3" {
  source               = "../modules/cloud_function"
  bucket_name          = "oanda_update_account_bucket"
  bucket_archive_name  = "oanda_update_account_archive"
  local_path           = "../functions/oanda_update_account.zip"
  function_name        = "oanda_update_account"
  function_runtime     = "nodejs10"
  function_memory      = 128
  function_entry_point = "run"
}
module "cloud_function_4" {
  source               = "../modules/cloud_function"
  bucket_name          = "oanda_update_candles_bucket"
  bucket_archive_name  = "oanda_update_candles_archive"
  local_path           = "../functions/oanda_update_candles.zip"
  function_name        = "oanda_update_candles"
  function_runtime     = "nodejs10"
  function_memory      = 128
  function_entry_point = "run"
}



















