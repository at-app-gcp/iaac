
variable "compute_name" {
    default = "compute default"
}

variable "compute_machine_type" {
    default = "n1-standard-1"
}

variable "compute_image" {
    default = "debian-cloud/debian-9"
}


variable "startup_script" {
    default = "> init_file.txt"
}





