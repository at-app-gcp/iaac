# resource "google_compute_instance" "main" {
#   name         = "${var.compute_name}"
#   machine_type = "${var.compute_machine_type}"
#   # zone         = "${var.zone}"

#   boot_disk {
#     initialize_params {
#       image = "${var.compute_image}"
#     }
#   }

#   network_interface {
#     network = "default"
#   }

#   metadata_startup_script = "${var.startup_script}"
#   tags = ["http-server", "https-server"]
#   service_account {
#     scopes = ["userinfo-email", "compute-ro", "storage-ro"]
#   }
# }

resource "google_compute_instance" "default" {
  name         = "test"
  machine_type = "n1-standard-1cd"
  zone         = "us-central1-a"

  tags = ["http-server", "https-server"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = "SCSI"
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    foo = "bar"
  }

  metadata_startup_script = "echo hi > /test.txt"

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}
# https://github.com/terraform-google-modules/terraform-google-container-vm/blob/master/examples/simple_instance/main.tf

# resource "google_compute_instance" "default" {
#   name         = "test"
#   machine_type = "e2-medium"
#   zone         = "us-central1-a"

#   tags = ["foo", "bar"]

#   boot_disk {
#     initialize_params {
#       image = "debian-cloud/debian-9"
#     }
#   }

#   // Local SSD disk
#   scratch_disk {
#     interface = "SCSI"
#   }

#   network_interface {
#     network = "default"

#     access_config {
#       // Ephemeral IP
#     }
#   }

#   metadata = {
#     foo = "bar"
#   }

#   metadata_startup_script = "echo hi > /test.txt"

#   service_account {
#     scopes = ["userinfo-email", "compute-ro", "storage-ro"]
#   }
# }