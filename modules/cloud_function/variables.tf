############################
###        FUNCTION      ###
############################

variable "function_name" {
  default = "fx_name" 
  description = "A user-defined name of the function. Function names must be unique globally"
}

variable "function_memory" {
  default     = 128
  description = "Memory (in MB), available to the function. Allowed values are: 128MB, 256MB, 512MB, 1024MB, and 2048MB"
}

variable "function_timeout" {
  default     = 30
  description = "Timeout (in seconds) for the function. Default value is 60 seconds. Cannot be more than 540 seconds"
}

variable "function_entry_point" {
  default     = "run" 
  description = "Name of a JavaScript function that will be executed when the Google Cloud Function is triggered"
}
variable "function_runtime" {
  default = "node10js"
  description = "Necuted when the Google Cloud Function is triggered"
}

variable "region" {
  default = "europe-west3"
}

variable "project" {
  default = "at202xcloud"
}

variable "source_dir" {
  default = ""
}
############################
###        BUCKET        ###
############################

variable "bucket_name" {
  default = "default bucket"
  description = "The name of the containing bucket"
}

variable "bucket_archive_name" {
  default = "default archive bucket"
  description = "The name of the object"
}

############################
###        LOCAL         ###
############################

variable "local_path" {
  default = "./"
  description = "A path to the data you want to upload"
}


