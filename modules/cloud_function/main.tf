# resource "google_storage_bucket" "bucket24" {
#   name = "oanda_get_trade-bucket24"
# }

# resource "google_storage_bucket_object" "archive24" {
#   name   = "oanda_get_trade.zip"
#   bucket = google_storage_bucket.bucket24.name
#   source = "../functions/oanda_get_trade.zip"
# }

# resource "google_cloudfunctions_function" "function24" {
#   name        = "oanda_get_trade"
#   description = "oanda_get_trade cloud function"
#   runtime     = "nodejs10"

#   available_memory_mb   = 128
#   source_archive_bucket = google_storage_bucket.bucket24.name
#   source_archive_object = google_storage_bucket_object.archive24.name
#   trigger_http          = true
#   entry_point           = "run"
# }

# # IAM entry for all users to invoke the function
# resource "google_cloudfunctions_function_iam_member" "invoker" {
#   project        = google_cloudfunctions_function.function24.project
#   region         = google_cloudfunctions_function.function24.region
#   cloud_function = google_cloudfunctions_function.function24.name

#   role   = "roles/cloudfunctions.invoker"
#   member = "allUsers"
# }

######################################################## 
resource "google_storage_bucket" "bucket" {
  name = "${var.bucket_name}"
}

# data "archive_file" "http_trigger" {
#   type        = "zip"
#   output_path = "${path.module}/files/http_trigger.zip"
#   source {
#     content  = "${file("${path.module}/files/http_trigger.js")}"
#     filename = "index.js"
#   }
# }

# data "archive_file" "function_source_code" {
#  type        = "zip"
#  source_dir  = "../../../../functions/${source_dir}"
#  output_path = "../../development/${source_dir}"
# }

resource "google_storage_bucket_object" "archive" {
  name   = "${var.bucket_archive_name}"
  bucket = google_storage_bucket.bucket.name
  source = "${var.local_path}"
}

resource "google_cloudfunctions_function" "main" {
  name        = "${var.function_name}"
  runtime     = "${var.function_runtime}"

  available_memory_mb   = "${var.function_memory}"
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  entry_point           = "${var.function_entry_point}"
}



# resource "google_cloudfunctions_function" "main" {
#   # depends_on = [google_storage_bucket_object.archive]
#   name        = "${var.function_name}"
#   description = "${var.function_description}"
#   runtime     = "${var.function_runtime}"

#   available_memory_mb   = "${var.function_memory}"
#   source_archive_bucket = google_storage_bucket.bucket.name
#   source_archive_object = google_storage_bucket_object.archive.name
#   trigger_http          = true
#   entry_point           = "${var.function_entry_point}"
#   project               = "${var.project}"
#   region                = "${var.region}"
# }






resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.main.project
  region         = google_cloudfunctions_function.main.region
  cloud_function = google_cloudfunctions_function.main.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}