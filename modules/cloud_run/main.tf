resource "google_cloud_run_service" "main" {
  name     = "${var.cloud_run_name}"
  location = "${var.location}"

  template {
    spec {
      containers {
        image = "${var.cloud_run_image}"

      }
    }
  }
}

resource "google_cloud_run_service_iam_member" "allUsers" {
  service  = google_cloud_run_service.main.name
  location = google_cloud_run_service.main.location
  role     = "roles/run.invoker"
  member   = "allUsers"
}
