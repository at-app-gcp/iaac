variable "cloud_run_name" {
  default = ""
}

variable "location" {
  default = ""
}

variable "cloud_run_image" {
  default = ""
}